var activeFile = new Array;
var b;

// Parsing active.in
$.get("/data/active.in", function(data){
	data = data.split("\n");
	if (data[data.length - 1].trim() == "")
		data.pop();
	var i, j, temp = [];
	for (i = 0; i < data.length; i++ ) {
		var row = new Object();
						
		j = data[i].lastIndexOf(')');
		temp[2] = data[i].substr(j+1, data[i].length-1);
		temp[0] = data[i].substr(0,j);
		
		j = temp[0].lastIndexOf('(');
		temp[1] = temp[0].substr(j+1, data[i].length-1);
		temp[0] = temp[0].substr(0,j-1);
		
		row.description = temp[0].trim();
		row.variable = temp[1].trim();
		row.value = parseFloat(temp[2].trim());
		
		activeFile.push(row);
	}
			
	
	//$("#app").append('<table><tbody>');
	var len = activeFile.length;
	var j, i = 0;
	while (i < len) {
		$("#active_form").append('<div class="row"');
		for (j = 0; j < 4 && i+j < len; j++) {
			$("#active_form").append('<div class="col-sm-3"><p> ' + activeFile[i+j].variable +':<input type="number" title='+ 
				activeFile[i+j].description + ' id=act'+ i +' value=' + activeFile[i].value + '></p> </div>' );
			i++;
		}
		$("#active_form").append('</div><br>');
		//$("#active_form").append('<div class="col-sm-6" style="background-color:yellow;"><p>Lorem ipsum...</p>	</div>');
	}
		
				//$("#active_form").append("<tr><td>" + activeFile[i].variable +': <input type="number" id=act'+i+' value=' + activeFile[i].value + '></td></tr>' );
	//$("#app").append('<tr><td><input id="update" type="submit" value="Update"></td></tr>');
	
	$("#update").click(function() {
		for (var i = 0; i < activeFile.length; i++ ) {
			activeFile[i].value = document.getElementById("act"+i).value;
		}

		writeUpdatedActiveFile(activeFile);
		
		/*console.log("----");
		for (var i = 0; i < activeFile.length; i++ ) {
			console.log(activeFile[i].value);
		}*/
	});	
	
	
	function writeUpdatedActiveFile(data) 
	{
		var space = ' ';
		var n_spaces, str = '';
		
		for (var i = 0; i < data.length; i++ ) {
			n_spaces = 65 - (data[i].variable.length + 2) - data[i].description.length;
			str = str.concat(data[i].description + space.repeat(n_spaces) + '(' + data[i].variable + ')');
			n_spaces = 13 - data[i].value.length;
			str = str.concat( space.repeat(n_spaces) + data[i].value + '\n');
		}
		
		document.cookie = str;
		console.log(str);
		alert(document.cookie);
		/*var txtFile = new File("file.txt");
		txtFile.open("w");
		for (var i = 0; i < activeFile.length; i++ ) {
			txtFile.writeln(activeFile[i].value);
		}
		txtFile.close();	*/	
		
		
	}


	//$("#app").append('</table></tbody>');		
});	
