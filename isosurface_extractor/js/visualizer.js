var range = [];
var bounds = [];
var isovalue_selected;

//Load data from data_info.txt
$.get("/data/output_info.txt", function(data){
	data = data.split("\n");
	var i;
	data[1] = data[1].trim().replace(',',' ');
	for (i = 0; i < data.length; i++) {
		data[i] = data[i].split(" ");
		//console.log(data[i]);
	}
	isovalue_selected = parseFloat(data[0][1]);
	
	range[0] = parseFloat(data[1][1]);
	range[1] = parseFloat(data[1][3]);
	
	bounds[0] = parseFloat(data[2][1]);
	bounds[1] = parseFloat(data[2][3]);
	bounds[2] = parseFloat(data[3][1]);
	bounds[3] = parseFloat(data[3][3]);
	bounds[4] = parseFloat(data[4][1]);
	bounds[5] = parseFloat(data[4][3]);
	
	console.log(range);
	console.log(bounds);
});


var scene = new THREE.Scene();
var camera, renderer, material, render;

function visualize()  {

	var width_vis = getHorizontalSpaceFromWindow() - 5;
	var height_vis = getVerticalSpaceFromWindow() - 5;
	
	camera = new THREE.PerspectiveCamera( 70, width_vis/height_vis, 1, 5000 );
	//camera.position.x = (bounds[0]+bounds[1]);
	//camera.position.y = bounds[2]+bounds[3];
	camera.position.z = bounds[5]*2;
	console.log(camera.position);
	
	scene = new THREE.Scene();
	
	// LIGHTS

	var ambient = new THREE.AmbientLight( 0x363636 );
	scene.add( ambient );
	
	var x_size = bounds[1]-bounds[0];
	var y_size = bounds[3]-bounds[2];
	var z_size = bounds[5]-bounds[4];
	
	var light1 = new THREE.SpotLight( 0xffeedd, 1.0, 1000, Math.PI / 2 );
	light1.position.set( (x_size/2) * 1.5, (y_size/2) * 1.5, (z_size/2) * 1.5 );
	scene.add( light1 );
	
	var light2 = new THREE.SpotLight( 0xffeedd, 1.0, 1000, Math.PI / 2 );
	light2.position.set( (x_size/2) * (-1.5), (y_size/2) * (-1.5), (z_size/2) * (-1.5) );
	scene.add( light2 );
	
	// RENDERER
	
	renderer = new THREE.WebGLRenderer({antialias:true});
	renderer.setSize( width_vis, height_vis );
	document.getElementById("visualizer_box").appendChild( renderer.domElement );

	/*var pointLight = new THREE.PointLight( 0xffffff );
	light.position.set(-1000,2000,1000);
	scene.add( pointLight );
	

	var light = new THREE.PointLight(0xffffff);
	light.position.set(-100,200,100);
	scene.add(light);
	*/
	
	/*var objLoader = new THREE.OBJLoader();
	    var material = new THREE.MeshLambertMaterial({color: 0x55B663, side: THREE.DoubleSide});
	    objLoader.load('models/plane.obj', function (obj) {
		obj.traverse(function (child) {
		    if (child instanceof THREE.Mesh) {
			child.material = material;
		    }

		});
		scene.add(obj);
	    });*/
	    
	    
	// LOADER
	
	var loader = new THREE.CTMLoader(); 
	loader.load( "models/output.ctm",   function( geometry ) {
		material = new THREE.MeshLambertMaterial( { color: 0xffffff, reflectivity: 0.3 } );
		//material.wireframe = true;
		//material.vertexColors = THREE.VertexColors;
		material.transparent = false; 
		material.opacity = 0.7;
		
		//material.depthWrite = false;
		//material.depthTest = false;
		material.alphaTest = 0.5;
		//material.blending = ;
		
		//var face, numberOfSides, color;
		//console.log(geometry.getAttribute('vertices').array.length);
		
		
		//var bufferGeometry = THREE.BufferGeometryUtils.fromGeometry(geometry);
		/*for ( var i = 0; i < geometry.faces.length; i++ ) {
			face  = geometry.faces[ i ];	
			// determine if current face is a tri or a quad
			numberOfSides = ( face instanceof THREE.Face3 ) ? 3 : 4;
			// assign color to each vertex of current face
			for( var j = 0; j < numberOfSides; j++ ) 
			{
				vertexIndex = face[ faceIndices[ j ] ];
				// initialize color variable
				color = new THREE.Color( 0xffffff );
				color.setHex( Math.random() * 0xffffff );
				face.vertexColors[ j ] = color;
			}
		}
		*/
		callbackModel( geometry, 0, material, -(bounds[0]+bounds[1])/2, -(bounds[2]+bounds[3])/2, -(bounds[4]+bounds[5])/2, 0, 0 );

	}, { useWorker: false } ); 
	    
	    
	    

	// Add OrbitControls so that we can pan around with the mouse.
	controls = new THREE.OrbitControls(camera, renderer.domElement);

	render = function () {
		requestAnimationFrame( render );

		//cube.rotation.x += 0.01;
		//cube.rotation.y += 0.01;

		renderer.render(scene, camera);
		controls.update();
	};

	render();
	renderMenu();
	THREEx.WindowResize(renderer, camera);
}




function renderMenu() {
	
	//console.log(range[0]+" "+range[1]);
	
	// Add GUI	
	
	var FizzyText = function() {
		this.light1 = true;
		this.light2 = false;
		this.extract_isosurface = true;
		this.field = 'velocity';
		this.isovalue = isovalue_selected;
		this.wireframe = false;
		this.transparency = false;
		this.opacity = 0.7;
	};
	
	window.onload = function() {
		var menu = new FizzyText();
		gui = new DAT.GUI();
		
		gui.add(menu, "light1");
		gui.add(menu, "light2");
		gui.add(menu, "extract_isosurface");
		gui.add(menu, "isovalue").min(range[0]).max(range[1]).step(1);
		//gui.add(menu, 'field', ['velocity', 'pressure']);//.name('Material Type').listen();
		gui.add(menu, "wireframe").onChange(function(value) {
		    if (value) 
			material.wireframe = true;
		    else
			material.wireframe = false;
		});
		gui.add(menu, "transparency").onChange(function(value) {
		    if (value) 
			material.transparent = true;
		    else
			material.transparent = false;
		});
		gui.add(menu, "opacity").min(0).max(1).step(0.05).onChange(function(value) {
		   material.opacity = value;
		});
		//gui.add(menu, 'combo_box', { Basic:0, Lambert:1, Phong:2, Wireframe:3 } ).name('Material Type').listen();
		//gui.close();
	};
	
	
	
	// Listen to changes within the GUI
	/*gui.add(menu, "perspective").onChange(function(newValue) {
		console.log("Value changed to:  ", newValue);
	});
*/
	// Listen to changes outside the GUI - GUI will update when changed from outside
	//gui.add(obj, "name").listen();
}




function callbackModel( geometry, s, material, x, y, z, rx, ry ) {

	var mesh = new THREE.Mesh( geometry, material );
	
	mesh.position.set( x, y, z );
	//mesh.scale.set( s, s, s );
	//mesh.rotation.x = rx;
	//mesh.rotation.z = ry;

	mesh.castShadow = true;
	mesh.receiveShadow = true;
	
	console.log("ok");
	scene.add( mesh );

}


$(document).ready(visualize);

