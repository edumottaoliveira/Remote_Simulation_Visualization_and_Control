


var s3dData = [];
var s3dCategory = [];
// Parsing active.in
$.get("/data/s3d.in", function(data){
	data = data.split("\n");
	var i, j, k, temp = [];
	i = 0;
	k = -1;
	while (i < data.length) {
		if (data[i][0] == '=') {
			s3dCategory.push(data[i+1].trim());
			i += 3; 
			k++;
			s3dData[k] = [];
		}
		
		if (data[i].trim() != '') {
			j = data[i].indexOf(" - ");
			if (j != -1) {
				temp[0] = data[i].substr(0,j-1); 
				temp[1] = data[i].substr(j+3, data[i].length-1); 
			}
			else {
				temp[0] = ''; 
				temp[1] = data[i]; 
			}
			j = temp[1].lastIndexOf("(");
			if (j != -1) {
				temp[2] = temp[1].substr(j+1,temp[1].length-2).replace(')','');
				temp[1] = temp[1].substr(0, j-1);
			}
			else {
				temp[2] = '';
			}
			//console.log(temp);
			//temp = data[i].split("!");
			
			var row = {};
			row.value = temp[0].trim();
			row.description = temp[1].trim();
			row.variable = temp[2].trim();
			//row.variable = temp.lenght < 3? '' : temp[2].trim();
			s3dData[k].push(row);
		}
		i++;
	}
		
	for (i = 0; i < s3dCategory.length; i++) {
		//$("#s3d_form").append('<div class="row"');
		$("#s3d_form").append('<div class="col-sm-12"><p>'+capitalise(s3dCategory[i])+'</p></div>');
		var k = 0;
		j = 0;
		while (j < s3dData[i].length) {
			$("#s3d_form").append('<div class="row"');
			for (k = 0; k < 6 && j < s3dData[i].length; k++) {
				$("#s3d_form").append('<div class="col-sm-2"><i>'+s3dData[i][j].variable+'</i>: '+ s3dData[i][j].value+'</div>');
				j++;
			}
			$("#s3d_form").append('</div><br>');
		}	
	}	
});	



function capitalise(string) {
    return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
}
