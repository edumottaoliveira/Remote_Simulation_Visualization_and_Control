
var menu = ["active", "s3d", "bombmax", "bombmin", "visualizer"];
var colors = ['#026ae8', '#b60cd1', '#026ae8', '#deca02', '#e8611a'];

var browser_width = $(window).width();
var browser_height = $(window).height();

var content_width = getHorizontalSpaceContent();
var window_width = getHorizontalSpaceFromWindow();

d3.select(window).on('resize', resize); 

var gui; //Visualization control bar

function show_menu(m) {
	for (var i = 0; i < menu.length; i++)
		 document.getElementById(menu[i]).style.display = 'none';
	document.getElementById(m).style.display = 'block';
	$('#navbar').collapse('hide');
	
	if (m == 'visualizer')
		gui.open();
	else
		gui.close();
}


var make_button_active = function()
{
  //Get item siblings
  var siblings =($(this).siblings());

  //Remove active class on all buttons
  siblings.each(function (index)
    {
      $(this).removeClass('active');
    }
  )


  //Add the clicked button class
  $(this).addClass('active');
}

//Attach events to menu
$(document).ready(
  function()
  {
    $(".navbar-nav li").click(make_button_active);
  }  
)

function getHorizontalSpaceContent() {
	var e = document.documentElement,
	b = document.getElementsByTagName('body')[0],
	x_width = window.innerWidth || e.clientWidth || b.clientWidth;
	if (x_width > 1198)
		return 1150;
	else if (x_width > 990)
		return 960;
	else if (x_width > 766)
		return 727;
	else
		return x_width -10;
}

function getHorizontalSpaceFromWindow() {
	var e = document.documentElement,
	b = document.getElementsByTagName('body')[0],
	x_width = window.innerWidth || e.clientWidth || b.clientWidth;
	return x_width;
}

function getVerticalSpaceFromWindow() {
	var e = document.documentElement,
	b = document.getElementsByTagName('body')[0],
	height = window.innerHeight || e.clientHeight || b.clientHeight;
	return height;
}

function resize() {
	
    // update width
    x_width = getHorizontalSpaceContent();
    //console.log(x_width);
    
    maxGraph.width = x_width - maxGraph.margin.left - maxGraph.margin.right;
    maxGraph.xScale.range([0, maxGraph.width]);
    minGraph.width = maxGraph.width;
    minGraph.xScale.range([0, minGraph.width]);
    
    resizeMaxGraph();
    resizeMinGraph();
    
    // do the actual resize...
}









