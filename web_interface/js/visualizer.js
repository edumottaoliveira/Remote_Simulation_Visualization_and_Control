var VisualizerTab = function() {
}

var range = [];
var bounds = [];
var isovalue_selected;

//Load data from data_info.txt
$.get("/data/output_info.txt", function(data){
	data = data.split("\n");
	var i;
	data[1] = data[1].trim().replace(',',' ');
	for (i = 0; i < data.length; i++) {
		data[i] = data[i].split(" ");
		//console.log(data[i]);
	}
	isovalue_selected = parseFloat(data[0][1]);
	
	range[0] = parseFloat(data[1][1]);
	range[1] = parseFloat(data[1][3]);
	
	bounds[0] = parseFloat(data[2][1]);
	bounds[1] = parseFloat(data[2][3]);
	bounds[2] = parseFloat(data[3][1]);
	bounds[3] = parseFloat(data[3][3]);
	bounds[4] = parseFloat(data[4][1]);
	bounds[5] = parseFloat(data[4][3]);
	
	console.log(range);
	console.log(bounds);
	
	$( "body" ).trigger( "data-loaded");
});


var scene = new THREE.Scene();
var camera, renderer, material, render;

function visualize()  {
	var width_vis = getHorizontalSpaceFromWindow() - 5;
	var height_vis = getVerticalSpaceFromWindow() - 5;
	
	$( "body" ).on( "data-loaded", function( event, data ) {
		camera = new THREE.PerspectiveCamera( 70, width_vis/height_vis, 1, 5000 );
		camera.position.z = bounds[5]*2;
		//console.log("camera ready");
		
		scene = new THREE.Scene();
		
		// LIGHTS

		var ambient = new THREE.AmbientLight( 0x363636 );
		scene.add( ambient );
		
		var x_size = bounds[1]-bounds[0];
		var y_size = bounds[3]-bounds[2];
		var z_size = bounds[5]-bounds[4];
		
		var light1 = new THREE.SpotLight( 0xffeedd, 1.0, 1000, Math.PI / 2 );
		light1.position.set( (x_size/2) * 1.5, (y_size/2) * 1.5, (z_size/2) * 1.5 );
		scene.add( light1 );
		
		var light2 = new THREE.SpotLight( 0xffeedd, 1.0, 1000, Math.PI / 2 );
		
		light2.position.set( (x_size/2) * (-1.5), (y_size/2) * (-1.5), (z_size/2) * (-1.5) );
		scene.add( light2 );
		
		
		// RENDERER
		
		renderer = new THREE.WebGLRenderer({antialias:true});
		renderer.setSize( width_vis, height_vis );
		document.getElementById("visualizer_box").appendChild( renderer.domElement );
	
	    
		// LOADER
		
		var loader = new THREE.CTMLoader(); 
		loader.load( "models/output.ctm",   function( geometry ) {
			material = new THREE.MeshLambertMaterial( { color: 0xffffff, reflectivity: 0.3 } );
			material.transparent = false; 
			material.opacity = 0.7;
			material.alphaTest = 0.5;
			callbackModel( geometry, 0, material, -(bounds[0]+bounds[1])/2, -(bounds[2]+bounds[3])/2, -(bounds[4]+bounds[5])/2, 0, 0 );
		}, { useWorker: false } );

		// Add OrbitControls so that we can pan around with the mouse.
		controls = new THREE.OrbitControls(camera, renderer.domElement);

		render = function () {
			requestAnimationFrame( render );
			renderer.render(scene, camera);
			controls.update();
		};

		render();
		renderMenu();
		THREEx.WindowResize(renderer, camera);
	});
}


function renderMenu() {
	
	//console.log(range[0]+" "+range[1]);
	
	// Add GUI	
	
	var FizzyText = function() {
		this.light1 = true;
		this.light2 = false;
		this.extract_isosurface = true;
		this.combo_box = 'Basic';
		this.isovalue = isovalue_selected;
		this.wireframe = false;
		this.transparency = false;
		this.opacity = 0.7;
	};
	
	//window.onload = function() {
		var menu = new FizzyText();
		gui = new DAT.GUI();
		
		gui.add(menu, "light1");
		gui.add(menu, "light2");
		gui.add(menu, "extract_isosurface");
		gui.add(menu, "isovalue").min(range[0]).max(range[1]).step(1);
		gui.add(menu, 'combo_box', [ 'Basic', 'Lambert'] );//.name('Material Type').listen();
		gui.add(menu, "wireframe").onChange(function(value) {
		    if (value) 
			material.wireframe = true;
		    else
			material.wireframe = false;
		});
		gui.add(menu, "transparency").onChange(function(value) {
		    if (value) 
			material.transparent = true;
		    else
			material.transparent = false;
		});
		gui.add(menu, "opacity").min(0).max(1).step(0.05).onChange(function(value) {
		   material.opacity = value;
		});
		//gui.add(menu, 'combo_box', { Basic:0, Lambert:1, Phong:2, Wireframe:3 } ).name('Material Type').listen();
		gui.close();
	//};
	

}




function callbackModel( geometry, s, material, x, y, z, rx, ry ) {

	var mesh = new THREE.Mesh( geometry, material );
	
	mesh.position.set( x, y, z );

	mesh.castShadow = true;
	mesh.receiveShadow = true;
	
	scene.add( mesh );

}


$(document).ready(visualize);

