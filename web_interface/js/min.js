//Min Graph object
var minGraph = {};
minGraph.data =[];
minGraph.fields = [];

//Load data bomb.max.dat to minGraph.data and minGraph.fields
$.get("/data/bomb.min.dat", function(data){
	data = data.split("\n");
	if (data[data.length - 1].trim() == "")
		data.pop();
	var i, j, len, temp = [];
	data[0] = data[0].replace('#','').trim();
	data[0] = data[0].replace(/\s\s+/g,'!'); // remove extra spaces
	minGraph.fields = data[0].split("!");
	len = minGraph.fields.length;
	for (i = 0; i < len; i++)
		minGraph.data[i] = [];

	for (i = 2; i < data.length; i++ ) {
		data[i] = data[i].trim().replace(/\s+/g,'!');
		temp = data[i].split("!");
		minGraph.data[0].push(temp[0]);				
		for (j = 1; j < len; j++)
			minGraph.data[j].push(parseFloat(temp[j]));
	}
	
	// Defining a selection box
	for (i = 2; i < len-2; i++)
		$("#min-var-select").append("<option value="+i+">"+minGraph.fields[i]+ "</option>");
	$("#min-time-select").append("<option value="+0+">"+minGraph.fields[0]+ "</option>");
	$("#min-time-select").append("<option value="+1+">"+minGraph.fields[1]+ "</option>");
	
	plotMinGraph(2, 0);	
});
    
minGraph.margin = {top: 50, right: 20, bottom: 20, left: 60};
minGraph.width = content_width - minGraph.margin.left - minGraph.margin.right;
minGraph.height = 250 - minGraph.margin.top - minGraph.margin.bottom;
minGraph.xScale = d3.scale.linear();
minGraph.yScale = d3.scale.linear();
minGraph.listGraphs =[];	


function plotMinGraph(variable, time) {
	minGraph.listGraphs.push(variable);
	//var indexOption = $("#elementId").val(); //the value of the selected option
	//var data = [3, 6, 2, 7, 5, 2, 0, 3, 8, 9, 2, 5, 9, 3, 6, 3, 6, 2, 7, 5, 2, 1, 3, 8, 9, 2, 5, 9, 2, 7];
	var data = minGraph.data[variable];
	var time = minGraph.data[time];
	
	// Get the lower and highter value of the data
	var maxY = Math.max.apply(null,data) *1.2;
	var minY = Math.min.apply(null,data);
	var maxX = Math.max.apply(null,time);
	var minX = Math.min.apply(null,time);


	minGraph.xScale.domain([0, data.length]) //.domain([minX, maxX])
	.range([0, minGraph.width]);

	minGraph.yScale.domain([minY, maxY])
	.range([minGraph.height, 0]);

	var area = d3.svg.area()
	.x(function(d,i) { return minGraph.xScale(i); })
	.y0(minGraph.height)
	.y1(function(d) { return minGraph.yScale(d); });
	//.interpolate("linear");	
	
	var line = d3.svg.line()
	//.x(function(d,i) { return x(time[i]); })
	.x(function(d,i) { return minGraph.xScale(i); })
	.y(function(d) { return minGraph.yScale(d); })

	
	var graph = d3.select("#min_graph").append("svg:svg")
	.attr("width", minGraph.width + minGraph.margin.left + minGraph.margin.right)
	.attr("height", minGraph.height + minGraph.margin.top + minGraph.margin.bottom)
	.append("svg:g")
	.attr("transform", "translate(" + minGraph.margin.left + "," + minGraph.margin.top + ")")
	.attr("preserveAspectRatio", "xMinYMin meet")
	.attr("viewBox", "0 0 "+content_width+" 200")
	.classed("svg-content", true);
	
	graph.append("rect")
	.attr("width", minGraph.width)
	.attr("height", minGraph.height)
	.attr("fill", "white");
	
	var xAxis = d3.svg.axis()
	.scale(minGraph.xScale)
	//.tickSize(-height)
	//.tickSubdivide(true)
	.orient("bottom")
	.innerTickSize(-minGraph.height)
	.outerTickSize(0)
	.tickPadding(10);

	var yAxis = d3.svg.axis()
	.scale(minGraph.yScale)
	.orient("left")
	//.ticks(10)
	.tickFormat(function(d) {return d.toExponential(2);})
	.innerTickSize(-minGraph.width)
	.outerTickSize(0)
	.tickPadding(10);
	
	//x.domain(d3.extent(minGraph.data, function(d) { return d.date; }));
	//y.domain(d3.extent(minGraph.data, function(d) { return d.close; }));
	
	graph.append("path")
	.datum(data)
        .attr("class", "area")
        .attr("d", area);
	
	graph.append("svg:g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + minGraph.height + ")")
	.call(xAxis);

	graph.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end");
	//.text("Price ($)");
	
	// Adding line
	 graph.append("svg:path")
	.attr("class", "line")
	//.style({stroke: colors[variable%colors.length]})
	.style({stroke: "#026ae8"})
	.attr("d", line(data));
	
	
	graph.append("text")
        .attr("x", (minGraph.width / 2))             
        .attr("y", 0 - (minGraph.margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px")
        .style("text-decoration", "underline")  
        .text(minGraph.fields[variable]);        
}


function addMinGraph() {
	var e = document.getElementById('min-var-select');
	var variable = e.options[e.selectedIndex].value;
	//e = document.getElementById('max-time-select');
	//var time = e.options[e.selectedIndex].value;
	
	plotMinGraph(variable, 0);
}


function clearMinGraph() {
	//d3.select("#max_graph > svg").remove();
	minGraph.listGraphs =[];
	$("#min_graph > svg").remove();
}


//Update width information
function resizeMinGraph() {
	var listGraphs = minGraph.listGraphs;
	clearMinGraph();
	var i;
	for (i = 0; i < listGraphs.length; i++)
		plotMinGraph(listGraphs[i], 0);	
}

//Simulate data update through data shifting
function updateMinGraph() {
	var num_lines = minGraph.data[0].length;
	var i, data2;
	for (i = 0; i < minGraph.data.length; i++) {
		data2 = minGraph.data[i].slice(num_lines - 5, num_lines);
		data2 = data2.concat( minGraph.data[i].slice(0, num_lines - 5) );
		minGraph.data[i] = data2;
	}
	resizeMinGraph();
}
