
// Class with all the visualization functions
var simulationVisualization = function() {
	
	// Member variables
	//this.color_palette = ['#026ae8', '#b60cd1', '#026ae8', '#deca02', '#e8611a'];
	this.content_width = getHorizontalSpaceContent();
	this.window_width = getHorizontalSpaceFromWindow();
	
	this.top_menu_bar = new TopMenuBar(); 
	this.active_tab = new ActiveTab();
	this.input_tab = new InputTab();
	this.maxgraph_tab = new GraphTab();
	this.mingraph_tab = new GraphTab();
	this.visualizer_tab = new VisualizerTab();	
}

function initialize_min_graph() {
	console.log("test");
}


// TopMenuBar Class definition
var TopMenuBar = function() {
	this.tabs = ["active", "s3d", "bombmax", "bombmin", "visualizer"];
	this.initializeTabs(this.tabs);	
};

TopMenuBar.showTab = function(t, tabs) {
	for (var i = 0; i < tabs.length; i++)
		 document.getElementById(tabs[i]).style.display = 'none';
	document.getElementById(t).style.display = 'block';
	$('#navbar').collapse('hide');
	
	if (t == 'visualizer')
		gui.open();
	else
		gui.close();
};

TopMenuBar.prototype.initializeTabs = function(tabs) {
	document.getElementById("active_tab").addEventListener("click", function(){ TopMenuBar.showTab("active", tabs)});
	document.getElementById("input_tab").addEventListener("click", function(){ TopMenuBar.showTab("s3d", tabs)});
	document.getElementById("maxgraph_tab").addEventListener("click", function(){ TopMenuBar.showTab("bombmax", tabs)});
	document.getElementById("mingraph_tab").addEventListener("click", function(){ TopMenuBar.showTab("bombmin", tabs)});
	document.getElementById("visualizer_tab").addEventListener("click", function(){ TopMenuBar.showTab("visualizer", tabs)});
}






var InputTab = function() {
	
}

var GraphTab = function() {
	d3.select(window).on('resize', resize);
}





//var top_menu_bar = ["active", "s3d", "bombmax", "bombmin", "visualizer"];


var content_width = getHorizontalSpaceContent();
var window_width = getHorizontalSpaceFromWindow();



var gui; //Visualization control bar




var make_button_active = function()
{
  //Get item siblings
  var siblings =($(this).siblings());

  //Remove active class on all buttons
  siblings.each(function (index)
    {
      $(this).removeClass('active');
    }
  )


  //Add the clicked button class
  $(this).addClass('active');
}

//Attach events to top_menu_bar
$(document).ready(
  function()
  {
    $(".navbar-nav li").click(make_button_active);
  }  
)

function getHorizontalSpaceContent() {
	var e = document.documentElement,
	b = document.getElementsByTagName('body')[0],
	x_width = window.innerWidth || e.clientWidth || b.clientWidth;
	if (x_width > 1198)
		return 1150;
	else if (x_width > 990)
		return 960;
	else if (x_width > 766)
		return 727;
	else
		return x_width -10;
}

function getHorizontalSpaceFromWindow() {
	var e = document.documentElement,
	b = document.getElementsByTagName('body')[0],
	x_width = window.innerWidth || e.clientWidth || b.clientWidth;
	return x_width;
}

function getVerticalSpaceFromWindow() {
	var e = document.documentElement,
	b = document.getElementsByTagName('body')[0],
	height = window.innerHeight || e.clientHeight || b.clientHeight;
	return height;
}

function resize() {
	
    // update width
    x_width = getHorizontalSpaceContent();
    //console.log(x_width);
    
    maxGraph.width = x_width - maxGraph.margin.left - maxGraph.margin.right;
    maxGraph.xScale.range([0, maxGraph.width]);
    minGraph.width = maxGraph.width;
    minGraph.xScale.range([0, minGraph.width]);
    
    resizeMaxGraph();
    resizeMinGraph();
    
    // do the actual resize...
}









