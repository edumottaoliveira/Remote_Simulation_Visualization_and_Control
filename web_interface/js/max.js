//Max Graph object
var maxGraph = {};
maxGraph.data =[];
maxGraph.fields = [];

//Load data bomb.max.dat to maxGraph.data and maxGraph.fields
$.get("/data/bomb.max.dat", function(data){
	data = data.split("\n");
	if (data[data.length - 1].trim() == "")
		data.pop();
	var i, j, len, temp = [];
	data[0] = data[0].replace('#','').trim();
	data[0] = data[0].replace(/\s\s+/g,'!'); // remove extra spaces
	maxGraph.fields = data[0].split("!");
	len = maxGraph.fields.length;
	for (i = 0; i < len; i++)
		maxGraph.data[i] = [];

	for (i = 2; i < data.length; i++ ) {
		data[i] = data[i].trim().replace(/\s+/g,'!');
		temp = data[i].split("!");
		maxGraph.data[0].push(temp[0]);				
		for (j = 1; j < len; j++)
			maxGraph.data[j].push(parseFloat(temp[j]));
	}
	
	// Defining a selection box
	for (i = 2; i < len-2; i++)
		$("#max-var-select").append("<option value="+i+">"+maxGraph.fields[i]+ "</option>");
	$("#max-time-select").append("<option value="+0+">"+maxGraph.fields[0]+ "</option>");
	$("#max-time-select").append("<option value="+1+">"+maxGraph.fields[1]+ "</option>");
	
	plotMaxGraph(2, 0);	
});
    
maxGraph.margin = {top: 50, right: 20, bottom: 20, left: 60};
maxGraph.width = content_width - maxGraph.margin.left - maxGraph.margin.right;
maxGraph.height = 250 - maxGraph.margin.top - maxGraph.margin.bottom;
maxGraph.xScale = d3.scale.linear();
maxGraph.yScale = d3.scale.linear();
maxGraph.listGraphs =[];	


function plotMaxGraph(variable, time) {
	maxGraph.listGraphs.push(variable);
	//var indexOption = $("#elementId").val(); //the value of the selected option
	//var data = [3, 6, 2, 7, 5, 2, 0, 3, 8, 9, 2, 5, 9, 3, 6, 3, 6, 2, 7, 5, 2, 1, 3, 8, 9, 2, 5, 9, 2, 7];
	var data = maxGraph.data[variable];
	var time = maxGraph.data[time];
	
	// Get the lower and highter value of the data
	var maxY = Math.max.apply(null,data) *1.2;
	var minY = Math.min.apply(null,data);
	var maxX = Math.max.apply(null,time);
	var minX = Math.min.apply(null,time);
	

	maxGraph.xScale.domain([0, data.length]) //.domain([minX, maxX])
	.range([0, maxGraph.width]);

	maxGraph.yScale.domain([minY, maxY])
	.range([maxGraph.height, 0]);

	var area = d3.svg.area()
	.x(function(d,i) { return maxGraph.xScale(i); })
	.y0(maxGraph.height)
	.y1(function(d) { return maxGraph.yScale(d); });
	//.interpolate("linear");	
	
	var line = d3.svg.line()
	//.x(function(d,i) { return x(time[i]); })
	.x(function(d,i) { return maxGraph.xScale(i); })
	.y(function(d) { return maxGraph.yScale(d); })

	
	var graph = d3.select("#max_graph").append("svg:svg")
	.attr("width", maxGraph.width + maxGraph.margin.left + maxGraph.margin.right)
	.attr("height", maxGraph.height + maxGraph.margin.top + maxGraph.margin.bottom)
	.append("svg:g")
	.attr("transform", "translate(" + maxGraph.margin.left + "," + maxGraph.margin.top + ")")
	.attr("preserveAspectRatio", "xMinYMin meet")
	.attr("viewBox", "0 0 "+content_width+" 200")
	.classed("svg-content", true);
	
	graph.append("rect")
	.attr("width", maxGraph.width)
	.attr("height", maxGraph.height)
	.attr("fill", "white");
	
	var xAxis = d3.svg.axis()
	.scale(maxGraph.xScale)
	//.tickSize(-height)
	//.tickSubdivide(true)
	.orient("bottom")
	.innerTickSize(-maxGraph.height)
	.outerTickSize(0)
	.tickPadding(10);

	var yAxis = d3.svg.axis()
	.scale(maxGraph.yScale)
	.orient("left")
	//.ticks(10)
	.tickFormat(function(d) {return d.toExponential(2);})
	.innerTickSize(-maxGraph.width)
	.outerTickSize(0)
	.tickPadding(10);
	
	//x.domain(d3.extent(maxGraph.data, function(d) { return d.date; }));
	//y.domain(d3.extent(maxGraph.data, function(d) { return d.close; }));
	
	graph.append("path")
	.datum(data)
        .attr("class", "area")
        .attr("d", area);
	
	graph.append("svg:g")
	.attr("class", "x axis")
	.attr("transform", "translate(0," + maxGraph.height + ")")
	.call(xAxis);

	graph.append("g")
	.attr("class", "y axis")
	.call(yAxis)
	.append("text")
	.attr("transform", "rotate(-90)")
	.attr("y", 6)
	.attr("dy", ".71em")
	.style("text-anchor", "end");
	//.text("Price ($)");
	
	// Adding line
	 graph.append("svg:path")
	.attr("class", "line")
	//.style({stroke: colors[variable%colors.length]})
	.style({stroke: "#026ae8"})
	.attr("d", line(data));
	
	
	graph.append("text")
        .attr("x", (maxGraph.width / 2))             
        .attr("y", 0 - (maxGraph.margin.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px")
        .style("text-decoration", "underline")  
        .text(maxGraph.fields[variable]);        
}


function addMaxGraph() {
	var e = document.getElementById('max-var-select');
	var variable = e.options[e.selectedIndex].value;
	//e = document.getElementById('max-time-select');
	//var time = e.options[e.selectedIndex].value;
	
	plotMaxGraph(variable, 0);
}


function clearMaxGraph() {
	//d3.select("#max_graph > svg").remove();
	maxGraph.listGraphs =[];
	$("#max_graph > svg").remove();
}






//Update width information
function resizeMaxGraph() {
	var listGraphs = maxGraph.listGraphs;
	clearMaxGraph();
	var i;
	for (i = 0; i < listGraphs.length; i++)
		plotMaxGraph(listGraphs[i], 0);	
}

//Simulate data update through data shifting
function updateMaxGraph() {
	var num_lines = maxGraph.data[0].length;
	var i, data2;
	for (i = 0; i < maxGraph.data.length; i++) {
		data2 = maxGraph.data[i].slice(num_lines - 5, num_lines);
		data2 = data2.concat( maxGraph.data[i].slice(0, num_lines - 5) );
		maxGraph.data[i] = data2;
	}
	resizeMaxGraph();
}
