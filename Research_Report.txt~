---------------------------------
        RESEARCH REPORT
---------------------------------
Author: Eduardo Motta de Oliveira
Contact: emottadeoliveira@ucdavis.edu


-----------------
 BEFORE 25 JUL:
-----------------

- Implemented a web interface to visualize an example file of a running scientific simulation. The web interface consists in:
    . A screen showing the parameters in the active.in file, including text boxes to change their values and a button to update these values to a local file;
    . A screen presenting the input values in the s3d.in file, defined in the beginning of the simulation;
    . Two screens showing the max and min values in line graphs of the variables in the files bomb.max.dat and bomb.min.dat. Buttons to add a graph for each variable and clear the screen were also included;
    . A screen with a visualizer implemented in three.js, allowing the user to navigate trough the model generated during the simulation;

- The base interface (menu, buttons, text boxes) was implemented with bootstrap to make easier the resizing of the contents inside the browser window.

-----------------
 25 JUL - 1 AUG:
-----------------

- Created a repository for the code. Gitlab was chosen because is a free service that allows private projects;
- Created a profile in Trello and a "To Do List" to make easier the control and managing of the project. All the involved researchers were added;
- Code updated to make plots to resize properly with the window;
- An experimental update feature was implemented and tested in the graph screens. It allows the user to update the graphs with update data from the visualization processing;
- Background, gridlines, and axes were added to give the interface a better aesthetical appearance;
- Added a UI panel to the visualization window. The dat.GUI library, because of its simple implementation and good appearance, were used to implement this feature; 
- Researched the best mesh formats that can be used to transfer the generated model. Open3DGC and OpenCTM were the best options found.

-----------------
 1 AUG - 8 AUG:
-----------------

- Created this Research Report with previous tasks executed in this project;
- OpenCTM were chosen as the final format for mesh streaming due its high portability and compression. Open3DGC converter just supports COLLADA files as input, for example, while the Open3DGC converter works with eight different input formats;
- Tests executed by generating an isosurface in OBJ format based in an input scalar field in VTK format, converting the isosurface in a compressed OpenCTM file and decoding the OpenCTM file with JavaScript in the visualization interface;
- Researched ways to transfer the isosurface mesh through the network.

-----------------
 8 AUG - 15 AUG:
-----------------

- Added a slider in the visualization controls to show the min, the max and the current isovalue selected;
- Added the output file "output_info.txt" to the isosurface extraction process. This file describes the bounding box of the model and show the current isovalue selected;
- Implemented the automated translating of the model and positioning of the camera, based on the bounding box described in the "output_info.txt" file;
- Researched about order-independent transparency in Three.js. Unfortunately, Three.js do not offer this functionality already implemented and its implementation is complex and need to be made manually (no third party library found);
- Added the basic transparency offered by Three.js in the visualization screen. Buttons were also added in the control bar to offer interaction with the final user;
- Researched about how add color vertices to the isosurface.

-----------------
 15 AUG - 31 AUG:
-----------------

- Code reorganized in a oriented object structure.
